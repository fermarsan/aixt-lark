

## os module

### `input()` function

The input strings to be captured by the `input()` function have a fixed size of 30 characters.