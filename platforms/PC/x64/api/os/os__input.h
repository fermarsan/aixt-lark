// This file is part of the Aixt project, https://gitlab.com/fermarsan/aixt-project.git
//
// The MIT License (MIT)
// 
// Copyright (c) 2022-2023 Fernando Martínez Santa

#ifndef _OS__INPUT_H_
#define _OS__INPUT_H_

#include <stdio.h>
#include <string.h>

#define OUT = os__input(IN) 

char in_str[30];

char *os__input(char *msg);

#endif  // _OS__INPUT_H_